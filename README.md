#Campaign API

## Get Started
1. **Clone o repositorio.** - `git clone https://yourusername@bitbucket.org/felipemonteverde/campaign-api.git`).
2. **Import o projeto no Eclipse como Gradle.**.
3. **Para o deploy da aplicacao num servidor, exemplo tomcat.**.
4. ** Acesse o Swagger com a documentaçáo das apis. - http://localhost:8080/people-api/swagger-ui.html)..**

## Technologies
- Java 8
- Spring Boot
- Tomcat
- Spring Data
- MySQL
- JPA
- Swagger
- Maven
- Gladle
- JUnit

## Configurações
-script.sql: Carga de dados inicial da aplicação. 