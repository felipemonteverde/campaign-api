package com.campaign.exception;

public class BusinessException extends Exception {

    private static final long serialVersionUID = 1149241039409861914L;

   
    public BusinessException(String msg){
        super(msg);
    }

    // contr�i um objeto NumeroNegativoException com mensagem e a causa dessa exce��o, utilizado para encadear exceptions
    public BusinessException(String msg, Throwable cause){
        super(msg, cause);
    }

}
