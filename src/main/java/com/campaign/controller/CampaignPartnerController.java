package com.campaign.controller;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.campaign.domain.ErrorMessage;
import com.campaign.dto.CampaignResponseDTO;
import com.campaign.dto.UserDTO;
import com.campaign.exception.BusinessException;
import com.campaign.service.ICampaignService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
@RequestMapping("partner-campaign")
public class CampaignPartnerController {
	
	@Autowired
	private ICampaignService campaignService;	

	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Create Partner Campaign", nickname = "Create Partner Campaign" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class),            
            @ApiResponse(code = 201, message = "Created", response = CampaignResponseDTO.class),
            @ApiResponse(code = 409, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.POST, path="/new", produces = "application/json")
	public @ResponseBody ResponseEntity createPartnerCampaign(@RequestHeader("userId") String userId, @RequestBody UserDTO user) {
	    try {
	    	campaignService.createPartnerCampaign(user, userId);
	        return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.CONFLICT);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Partner Campaigns", nickname = "Get Partner Campaigns" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class),                       
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)})	
	@RequestMapping(method = RequestMethod.GET, path="/all", produces = "application/json")
	public @ResponseBody ResponseEntity getPartnerCampaign(@RequestParam(value="userId") Integer userId) {
		try {
			List<CampaignResponseDTO> list = campaignService.getPartnerCampaigns(userId);
			return new ResponseEntity<List<CampaignResponseDTO>>(list, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
}
