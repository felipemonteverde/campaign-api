package com.campaign.controller;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.campaign.domain.ErrorMessage;
import com.campaign.dto.TeamDTO;
import com.campaign.exception.BusinessException;
import com.campaign.service.ITeamService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("team")
public class TeamController {

	@Autowired
	private ITeamService teamService;
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "List of Teams", nickname = "List Teams")
	@ApiResponses(value = {            
			@ApiResponse(code = 200, message = "Failure", response = TeamDTO.class, responseContainer="List"),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="all", produces = "application/json")
	public @ResponseBody ResponseEntity getAllTeams() {
		try {
			List<TeamDTO> list = teamService.getAllTeams();
			return new ResponseEntity<List<TeamDTO>>(list, HttpStatus.OK);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
		
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Team By Id", nickname = "Get Team By Id" )
	@ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Success", response = TeamDTO.class),           
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="/{id}", produces = "application/json")	
	public @ResponseBody ResponseEntity getTeamById(@PathVariable("id") int id) {
		try {
			TeamDTO team = teamService.getTeamById(id);
			return new ResponseEntity<TeamDTO>(team, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
