package com.campaign.controller;

import java.util.List;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.campaign.domain.ErrorMessage;
import com.campaign.dto.CampaignRequestDTO;
import com.campaign.dto.CampaignResponseDTO;
import com.campaign.entity.Campaign;
import com.campaign.exception.BusinessException;
import com.campaign.service.ICampaignService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Controller
@RestController
@RequestMapping("campaign")
public class CampaignController {

	@Autowired
	private ICampaignService campaignService;	
		
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Campaigns", nickname = "Get Campaigns" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class, responseContainer="List"),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="/all", produces = "application/json")
	public @ResponseBody ResponseEntity getAllCampaign(@RequestParam(value="teamId", required=false) Integer teamId) {
		try {
			List<CampaignResponseDTO> list = campaignService.getAllCampaigns(teamId);
			return new ResponseEntity<List<CampaignResponseDTO>>(list, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Modified Campaigns", nickname = "Get Modified Campaigns" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class, responseContainer="List"),            
            @ApiResponse(code = 404, message = "Not Found", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="all/modified", produces = "application/json")
	public @ResponseBody ResponseEntity getAllCampaignModifiedRecentily(@RequestParam(value="teamId", required=false) Integer teamId) {
		try {
			List<CampaignResponseDTO> list = campaignService.getAllCampaignsModifiedRecentily(teamId);
			return new ResponseEntity<List<CampaignResponseDTO>>(list, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
		
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Get Campaign By Id", nickname = "Get Campaign By Id" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class),
            @ApiResponse(code = 404, message = "Not Found", response = Void.class),            
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.GET, path="/{id}", produces = "application/json")	
	public @ResponseBody ResponseEntity getCampaignById(@PathVariable("id") int id) {
		try {
			CampaignResponseDTO campaign = campaignService.getCampaignById(id);
			return new ResponseEntity<CampaignResponseDTO>(campaign, HttpStatus.OK);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Create Campaign", nickname = "Create Campaign" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class),
            @ApiResponse(code = 201, message = "Criated", response = Void.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CampaignResponseDTO.class),
            @ApiResponse(code = 409, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.POST, path="/new", produces = "application/json")
	public @ResponseBody ResponseEntity createCampaign(@RequestHeader("userId") String userId, @RequestBody @Valid CampaignRequestDTO campaignReq, UriComponentsBuilder builder) {
	    try {
	    	Campaign campaign = campaignService.createCampaign(campaignReq, userId);	        
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(builder.path("/campaign/{id}").buildAndExpand(campaign.getCampaignId()).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.CONFLICT);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Remove Campaign", nickname = "Remove Campaign" )
	@ApiResponses(value = {			
            @ApiResponse(code = 204, message = "No Content", response = ErrorMessage.class),
            @ApiResponse(code = 404, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.DELETE, path="/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity deleteCampaign(@PathVariable("id") int id) {
		try {
			campaignService.deleteCampaign(id);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
	
	@SuppressWarnings("rawtypes")
	@ApiOperation(value = "Update Campaign", nickname = "Update Campaign" )
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Success", response = CampaignResponseDTO.class),
            @ApiResponse(code = 204, message = "No Content", response = Void.class),
            @ApiResponse(code = 400, message = "Bad Request", response = CampaignResponseDTO.class),
            @ApiResponse(code = 409, message = "Conflict", response = ErrorMessage.class),
            @ApiResponse(code = 500, message = "Failure", response = ErrorMessage.class)}) 
	@RequestMapping(method = RequestMethod.PUT, path="/{id}", produces = "application/json")
	public @ResponseBody ResponseEntity updateCampaign(@PathVariable("id") int id, @RequestHeader("userId") String userId, @RequestBody @Valid CampaignRequestDTO campaignReq) {
		try {						
			campaignService.updateCampaign(id, campaignReq, userId);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (BusinessException e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.NOT_FOUND);
		} catch (Exception e) {			
			return new ResponseEntity<ErrorMessage>(new ErrorMessage("0", e.getMessage(), new DateTime()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
}
