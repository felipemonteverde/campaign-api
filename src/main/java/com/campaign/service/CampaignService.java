package com.campaign.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.campaign.dao.ICampaignDao;
import com.campaign.dao.IPartnerCampaignDao;
import com.campaign.dao.ITeamDao;
import com.campaign.dto.CampaignRequestDTO;
import com.campaign.dto.CampaignResponseDTO;
import com.campaign.dto.UserDTO;
import com.campaign.entity.Campaign;
import com.campaign.entity.Team;
import com.campaign.entity.UserCampaign;
import com.campaign.exception.BusinessException;

@Service
public class CampaignService implements ICampaignService {
	
	@Autowired
	private ICampaignDao campaignDao;
	
	@Autowired
	private ITeamDao teamDao;
	
	@Autowired
	private IPartnerCampaignDao partnerCampaignDao;
			
	@Override
	public List<CampaignResponseDTO> getAllCampaigns(Integer teamId) throws BusinessException {		
		List<Campaign> campaigns = getCampaigns(teamId);
		List<CampaignResponseDTO> list = new ArrayList<CampaignResponseDTO>();
		for (Campaign campaign : campaigns) {
			list.add(new CampaignResponseDTO(campaign));
		}
		return list;
	}
	
	private List<Campaign> getCampaigns(Integer teamId) throws BusinessException {
		// Validate if Team exists
		if(teamId != null) {
			getTeam(teamId);
		}
		return campaignDao.getAllCampaigns(teamId);
	}

	@Override
	public CampaignResponseDTO getCampaignById(int campaignId) throws BusinessException {		
		Campaign campaign  = campaignDao.getCampaignById(campaignId);
		if(campaign == null) {
			throw new BusinessException("Cadastro nao encontrado "+ campaignId);
		}		
		return new CampaignResponseDTO(campaign);
	}

	@Override
	public Campaign createCampaign(CampaignRequestDTO campaignReq, String userId) throws BusinessException {		
		Campaign campaign = campaignReq.toCampaign();
		if(campaign.getTeam() != null && campaign.getTeam().getTeamId() > 0) {			
			campaign.setTeam(getTeam(campaign.getTeam().getTeamId()));
		}
		
		List<Campaign> campaigns = getCampaigns(campaign.getTeam().getTeamId());
		List<Campaign> updatedListCampaigns = adjustFinishDate(campaigns, campaign);
		for (Campaign c : updatedListCampaigns) {			
			campaignDao.updateCampaign(c);
			
		}
		campaign.setCreatedBy(userId);		
		campaignDao.createCampaign(campaign);		
		return campaign;
	}
	
	private List<Campaign> adjustFinishDate(List<Campaign> campaigns, Campaign newCampaign) {
		List<Campaign> list = campaigns;
		List<Date> listOfDates = new ArrayList<Date>();
		List<Campaign> updatedList = new ArrayList<Campaign>();		
		Date campaignFinishDate = newCampaign.getFinishDate();
		if (list != null && !list.isEmpty()) {
			for (Campaign c : list) {
				listOfDates.add(c.getFinishDate());
			}

			for (Campaign c : list) {
				listOfDates.remove(c.getFinishDate());
				LocalDate newFinishDate = new LocalDate(c.getFinishDate(), DateTimeZone.forTimeZone(TimeZone.getDefault()));
				c.setFinishDate(newFinishDate.plusDays(1).toDate());
				listOfDates.add(c.getFinishDate());
				c.setUpdatedBy("campaign");
				updatedList.add(c);
			}
			if (listOfDates.contains(campaignFinishDate)) {
				adjustFinishDate(updatedList, newCampaign);
			}
		}
		return list;
	}
		
	@Override
	public void updateCampaign(int id, CampaignRequestDTO campaignReq, String userId) throws BusinessException {		
		Campaign campaign = campaignReq.toCampaign();
		campaign.setCampaignId(id);
		if(campaign.getTeam() != null && campaign.getTeam().getTeamId() > 0) {			
			campaign.setTeam(getTeam(campaign.getTeam().getTeamId()));
		}
		campaign.setUpdatedBy(userId);
		campaignDao.updateCampaign(campaign);		
	}

	@Override
	public void deleteCampaign(int campaignId) throws BusinessException {		
		campaignDao.deleteCampaign(campaignId);		
	}

	@Override
	public List<CampaignResponseDTO> getAllCampaignsModifiedRecentily(Integer teamId) throws BusinessException {
		// Validate if Team exists
		if(teamId != null) {
			getTeam(teamId);
		}
		List<Campaign> campaigns = campaignDao.getAllCampaignsModifiedRecentily(teamId);
		
		List<CampaignResponseDTO> list = new ArrayList<CampaignResponseDTO>();
		for (Campaign campaign : campaigns) {
			list.add(new CampaignResponseDTO(campaign));
		}
		return list;
	}
	
	private Team getTeam(Integer teamId) throws BusinessException {
		Team team = null;
		if(teamId != null) {
			team = teamDao.getTeamById(teamId);
			if(team == null) {
				throw new BusinessException("Time invalido");
			}			
		}
		return team;
	}

	@Override
	public boolean createPartnerCampaign(UserDTO user, String userId) throws BusinessException {
		
		Team team = getTeam(user.getTeamId());
		List<Campaign> campaigns = getCampaigns(user.getTeamId());
		if(campaigns == null || campaigns.isEmpty()) {
			throw new BusinessException("Nao ha campanhas ativas para o time do coracao");
		}
		
		for (Campaign campaign : campaigns) {
			UserCampaign partnerCampaign = partnerCampaignDao.getPartnerCampaign(user.getUserId(), campaign.getCampaignId());
			
			UserCampaign userCampaign = new UserCampaign();
			userCampaign.setFullName(campaign.getName());
			userCampaign.setFinishDate(campaign.getFinishDate());
			userCampaign.setCampaignId(campaign.getCampaignId());
			userCampaign.setUserId(user.getUserId());
			userCampaign.setTeamId(team.getTeamId());						
			if(partnerCampaign == null) {
				userCampaign.setCreatedBy(userId);
				partnerCampaignDao.createPartnerCampaign(userCampaign);
			} else {				
				userCampaign.setUpdatedBy(userId);
				partnerCampaignDao.mergePartnerCampaign(userCampaign);
			}
		}
		
		return true;
	}

	@Override
	public List<CampaignResponseDTO> getPartnerCampaigns(Integer userId) throws BusinessException {		
		List<UserCampaign> partnerCampaigns = partnerCampaignDao.getPartnerCampaigns(userId);
		List<CampaignResponseDTO> campaigns = new ArrayList<CampaignResponseDTO>();
		for (UserCampaign partner : partnerCampaigns) {
			campaigns.add(new CampaignResponseDTO(campaignDao.getCampaignById(partner.getCampaignId())));
		}
		return campaigns;
		
	}
	
}
