package com.campaign.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.campaign.dao.ITeamDao;
import com.campaign.dto.TeamDTO;
import com.campaign.entity.Team;
import com.campaign.exception.BusinessException;

@Service
public class TeamService implements ITeamService{

	@Autowired
	private ITeamDao teamDao;
	
	@Override
	public List<TeamDTO> getAllTeams() {		
		List<Team> teams = teamDao.getAllTeams();
		List<TeamDTO> list = new ArrayList<>();
		for(Team team : teams) {
			list.add(new TeamDTO(team));
		}
		return list;
	}

	@Override
	public TeamDTO getTeamById(int teamId) throws BusinessException {
		Team team = teamDao.getTeamById(teamId);
		if(team == null)
			return null;
		return new TeamDTO(team);
	}

}
