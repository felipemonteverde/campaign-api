package com.campaign.service;

import java.util.List;

import com.campaign.dto.TeamDTO;
import com.campaign.exception.BusinessException;

public interface ITeamService {

	public List<TeamDTO> getAllTeams();
	public TeamDTO getTeamById(int teamId) throws BusinessException;
}
