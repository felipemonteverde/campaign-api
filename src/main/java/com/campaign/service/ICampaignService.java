package com.campaign.service;

import java.util.List;

import com.campaign.dto.CampaignRequestDTO;
import com.campaign.dto.CampaignResponseDTO;
import com.campaign.dto.UserDTO;
import com.campaign.entity.Campaign;
import com.campaign.exception.BusinessException;

public interface ICampaignService {
	
	public List<CampaignResponseDTO> getAllCampaigns(Integer teamId) throws BusinessException ;
	public CampaignResponseDTO getCampaignById(int campaignId) throws BusinessException;
    public Campaign createCampaign(CampaignRequestDTO campaignReq, String userId) throws BusinessException;
    public void updateCampaign(int id, CampaignRequestDTO campaign, String userId) throws BusinessException;
    public void deleteCampaign(int campaignId) throws BusinessException;
	public List<CampaignResponseDTO> getAllCampaignsModifiedRecentily(Integer teamId) throws BusinessException ;
	public boolean createPartnerCampaign(UserDTO user, String userId) throws BusinessException;
	public List<CampaignResponseDTO> getPartnerCampaigns(Integer userId) throws BusinessException;	
}
