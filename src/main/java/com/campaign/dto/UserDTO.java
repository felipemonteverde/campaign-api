package com.campaign.dto;

import io.swagger.annotations.ApiModelProperty;

public class UserDTO {

	@ApiModelProperty(notes = "The id of User")
	private Integer userId;
	@ApiModelProperty(notes = "The id of team")
	private Integer teamId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getTeamId() {
		return teamId;
	}
	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	
}
