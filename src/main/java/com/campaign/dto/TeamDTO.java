package com.campaign.dto;

import com.campaign.entity.Team;

import io.swagger.annotations.ApiModelProperty;

public class TeamDTO {

	@ApiModelProperty(notes = "The database generated teamId")
	private int teamId; 	
	@ApiModelProperty(notes = "The name of the team")
    private String name;
    
    public TeamDTO() {}
    public TeamDTO(Team team) {
    	this.teamId = team.getTeamId(); 	
        this.name= team.getName();
    }
    
	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
}
