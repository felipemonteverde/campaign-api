package com.campaign.dto;

import java.util.Date;

import com.campaign.entity.Campaign;

import io.swagger.annotations.ApiModelProperty;

public class CampaignResponseDTO {

	@ApiModelProperty(notes = "The database generated campaignId")
    private int campaignId; 
	@ApiModelProperty(notes = "The name of the campaign")
    private String name;	
	@ApiModelProperty(notes = "The start date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDate;
	@ApiModelProperty(notes = "The finish date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date finishDate;
	@ApiModelProperty(notes = "The team of the campaign")
	private TeamDTO team;
	@ApiModelProperty(notes = "User that create the campaign")
	private String createdBy;		
	@ApiModelProperty(notes = "Date that campaign's create")
	private Date createdAt;	
	@ApiModelProperty(notes = "User that updated the campaign")
	private String updatedBy;	
	@ApiModelProperty(notes = "Date that campaign's updated")
	private Date lastUpdateAt;
	
	public CampaignResponseDTO() {}
	public CampaignResponseDTO(Campaign campaign) {
		this.campaignId = campaign.getCampaignId(); 
		this.name = campaign.getName();	
		this.startDate = campaign.getStartDate();
		this.finishDate = campaign.getFinishDate();		
		this.createdBy = campaign.getCreatedBy();		
		this.createdAt = campaign.getCreatedAt();	
		this.updatedBy = campaign.getUpdatedBy();	
		this.lastUpdateAt = campaign.getLastUpdateAt();
		if(campaign.getTeam() != null) {			  
			this.team = new TeamDTO(campaign.getTeam());;
		}		
	}
	
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	public TeamDTO getTeam() {
		return team;
	}
	public void setTeamRequestDTO(TeamDTO team) {
		this.team = team;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getLastUpdateAt() {
		return lastUpdateAt;
	}
	public void setLastUpdateAt(Date lastUpdateAt) {
		this.lastUpdateAt = lastUpdateAt;
	}
	
}
