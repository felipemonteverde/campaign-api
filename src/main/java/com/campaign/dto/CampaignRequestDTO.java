package com.campaign.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.campaign.entity.Campaign;
import com.campaign.entity.Team;

import io.swagger.annotations.ApiModelProperty;

public class CampaignRequestDTO {
	
	@ApiModelProperty(notes = "The name of the campaign")
	@NotNull(message = "Field \"name\" is required!")  
	private String name;
	@ApiModelProperty(notes = "The start date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	@NotNull(message = "Field \"startDate\" is required!")
	private Date startDate;
	@ApiModelProperty(notes = "The finish date of the campaign in format yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	@NotNull(message = "Field \"finishDate\" is required!")
	private Date finishDate;
	@ApiModelProperty(notes = "The team of the campaign")
	@NotNull(message = "Field \"team\" is required!!")
	private TeamDTO team;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getFinishDate() {
		return finishDate;
	}
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	public TeamDTO getTeam() {
		return team;
	}
	public void setTeam(TeamDTO team) {
		this.team = team;
	}
	
	public Campaign toCampaign(){
		Campaign campaign = new Campaign();
		campaign.setName(this.name);
		campaign.setStartDate(this.startDate);
		campaign.setFinishDate(this.finishDate);
		if(this.team != null) {
			Team team = new Team();
			team.setTeamId(this.team.getTeamId());
			campaign.setTeam(team);
		}
		return campaign;
	}

}
