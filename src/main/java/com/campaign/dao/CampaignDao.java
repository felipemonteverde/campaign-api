package com.campaign.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.campaign.entity.Campaign;

@Transactional
public class CampaignDao implements ICampaignDao {

	@PersistenceContext	
	private EntityManager entityManager;
			
	@Override
	public List<Campaign> getAllCampaigns(Integer teamId) {
		if(teamId != null) {
			String hql = "FROM Campaign as c where c.team.teamId = ? AND c.finishDate > now() ORDER BY c.campaignId";
			return (List<Campaign>) entityManager.createQuery(hql).setParameter(1, teamId).getResultList();
			
		} else {
			String hql = "FROM Campaign as c where c.finishDate > now() ORDER BY c.campaignId";
			return (List<Campaign>) entityManager.createQuery(hql).getResultList();
		}
	}

	@Override
	public Campaign getCampaignById(int campaignId) {
		return entityManager.find(Campaign.class, campaignId);
	}

	@Override
	public void createCampaign(Campaign campaign) {			
		entityManager.persist(campaign);  	    
	}
	
	@Override
	public void updateCampaign(Campaign campaign) {
		Campaign campaignl = getCampaignById(campaign.getCampaignId());
		campaignl.setName(campaign.getName());
		campaignl.setStartDate(campaign.getStartDate());
		campaignl.setFinishDate(campaign.getFinishDate());
		campaignl.setUpdatedBy(campaign.getUpdatedBy());
		campaignl.setTeam(campaign.getTeam());
		entityManager.flush();	
	}

	@Override
	public void deleteCampaign(int campaignId) {
		entityManager.remove(getCampaignById(campaignId));	
	}

	@Override
	public List<Campaign> getAllCampaignsModifiedRecentily(Integer teamId) {
		if(teamId != null) {
			String hql = "FROM Campaign as c where c.team.teamId = ? AND c.finishDate > now() ORDER BY c.campaignId";
			return (List<Campaign>) entityManager.createQuery(hql).setParameter(1, teamId).getResultList();
		} else {
			String hql = "FROM Campaign as c where c.finishDate > now() and c.lastUpdateAt is not null ORDER BY c.campaignId";
			return (List<Campaign>) entityManager.createQuery(hql).getResultList();
		}
	}

}
