package com.campaign.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.campaign.entity.Team;

@Transactional
public class TeamDao implements ITeamDao {

	@PersistenceContext	
	private EntityManager entityManager;
	
	@Override
	public List<Team> getAllTeams() {		
		String hql = "FROM Team as t ORDER BY t.teamId";
		return (List<Team>) entityManager.createQuery(hql).getResultList();
	}
	
	@Override
	public Team getTeamById(int teamId) {
		return entityManager.find(Team.class, teamId);
	}
		
}
