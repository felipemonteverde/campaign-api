package com.campaign.dao;

import java.util.List;

import com.campaign.entity.Team;


public interface ITeamDao {
	public List<Team> getAllTeams();
	public Team getTeamById(int teamId);
}
