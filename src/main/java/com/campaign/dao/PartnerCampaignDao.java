package com.campaign.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.campaign.entity.UserCampaign;

@Transactional
public class PartnerCampaignDao implements IPartnerCampaignDao {

	@PersistenceContext	
	private EntityManager entityManager;
	
	@Override
	public void createPartnerCampaign(UserCampaign userCampaign) {		
		entityManager.persist(userCampaign);						
	}
	
	@Override
	public void mergePartnerCampaign(UserCampaign userCampaign) {		
		entityManager.merge(userCampaign);						
	}
	
	@Override
	public List<UserCampaign> getPartnerCampaigns(Integer userId) {
		String hql = "FROM UserCampaign AS uc WHERE uc.userId = ? ORDER BY uc.campaignId";
		return (List<UserCampaign>) entityManager.createQuery(hql).setParameter(1, userId).getResultList();
	}
	
	@Override
	public UserCampaign getPartnerCampaign(Integer userId, Integer campaignId) {
		String hql = "FROM UserCampaign AS uc WHERE uc.userId = ? and uc.campaignId = ?";
		List<UserCampaign> list = (List<UserCampaign>) entityManager.createQuery(hql).setParameter(1, userId).setParameter(2, campaignId).getResultList();
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
