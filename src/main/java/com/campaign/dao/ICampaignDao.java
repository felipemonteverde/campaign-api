package com.campaign.dao;

import java.util.List;

import com.campaign.entity.Campaign;

public interface ICampaignDao {
	public List<Campaign> getAllCampaigns(Integer teamId);
	public Campaign getCampaignById(int campaignId);
    public void createCampaign(Campaign campaign);
    public void updateCampaign(Campaign campaign);
    public void deleteCampaign(int campaignId);
	public List<Campaign> getAllCampaignsModifiedRecentily(Integer teamId);
}
