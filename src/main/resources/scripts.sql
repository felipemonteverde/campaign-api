CREATE DATABASE `campanhas` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;


CREATE TABLE `campaigns` (
  `campaign_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `team_id` int(5) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `last_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `teams` (`team_id`,`name`) VALUES (1,'Corinthians');
INSERT INTO `teams` (`team_id`,`name`) VALUES (2,'Palmeiras');
INSERT INTO `teams` (`team_id`,`name`) VALUES (3,'Santos');
INSERT INTO `teams` (`team_id`,`name`) VALUES (4,'Grêmio');
INSERT INTO `teams` (`team_id`,`name`) VALUES (5,'Cruzeiro');
INSERT INTO `teams` (`team_id`,`name`) VALUES (6,'Botafogo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (7,'Flamengo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (8,'Vasco');
INSERT INTO `teams` (`team_id`,`name`) VALUES (9,'Atlético-PR');
INSERT INTO `teams` (`team_id`,`name`) VALUES (10,'Atlético-MG');
INSERT INTO `teams` (`team_id`,`name`) VALUES (11,'São Paulo');
INSERT INTO `teams` (`team_id`,`name`) VALUES (12,'Bahia');
INSERT INTO `teams` (`team_id`,`name`) VALUES (13,'Fluminense');
INSERT INTO `teams` (`team_id`,`name`) VALUES (14,'Sport');
INSERT INTO `teams` (`team_id`,`name`) VALUES (15,'Avaí');
INSERT INTO `teams` (`team_id`,`name`) VALUES (16,'Chapecoense');
INSERT INTO `teams` (`team_id`,`name`) VALUES (17,'Vitória');
INSERT INTO `teams` (`team_id`,`name`) VALUES (18,'Ponte Preta');
INSERT INTO `teams` (`team_id`,`name`) VALUES (19,'Coritiba');
INSERT INTO `teams` (`team_id`,`name`) VALUES (20,'Atlético-GO');

INSERT INTO `campaigns` (`campaign_id`,`name`,`start_date`,`finish_date`,`team_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`) VALUES (1,'Campanha 1','2018-01-01','2018-10-05',1,'felipe','2018-03-25 15:23:40','campaign','2018-03-25 15:24:22');
INSERT INTO `campaigns` (`campaign_id`,`name`,`start_date`,`finish_date`,`team_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`) VALUES (2,'Campanha 2','2018-01-01','2018-10-04',1,'felipe','2018-03-25 15:24:15','campaign','2018-03-25 15:24:22');
INSERT INTO `campaigns` (`campaign_id`,`name`,`start_date`,`finish_date`,`team_id`,`created_by`,`created_at`,`updated_by`,`last_updated_at`) VALUES (3,'Campanha 3','2018-01-01','2018-10-03',2,'felipe','2018-03-25 15:24:22','felipe','2018-03-25 16:21:54');


